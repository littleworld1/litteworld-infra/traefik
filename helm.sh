#!/bin/bash

set -euo pipefail

HELM_NAMESPACE=traefik
HELM_NAME=traefik
HELM_CHART=traefik/traefik

helm repo add traefik https://helm.traefik.io/traefik
helm repo update

if [ $# -eq 0 ]; then
  echo "Use ./helm.sh show, ./helm.sh upgrade or ./helm.sh uninstall";
  exit 1
else
  case "$1" in
    "show")
      helm show values ${HELM_CHART} > values.yaml.example;
      exit 0
      ;;
    "upgrade")
      HELM_ACTION="upgrade --install"
      ;;
    "uninstall")
      helm uninstall ${HELM_NAME} --namespace ${HELM_NAMESPACE};
      exit 0
      ;;
    *)
      echo "Use ./helm.sh show, ./helm upgrade or ./helm.sh uninstall)"; 
      exit 1
      ;;
  esac
fi
echo "Do helm ${HELM_ACTION}"

helm ${HELM_ACTION} ${HELM_NAME} ${HELM_CHART} \
  --namespace ${HELM_NAMESPACE} \
  --create-namespace --wait --atomic --timeout 1m30s \
  --values=values.yaml

kubectl apply -f dashboard.yaml
